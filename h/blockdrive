/*
 * Copyright (c) 1992 Hugo Fiennes
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
/*                  _____________________________________________
                  [>                                             <]
Project           [> ARCterm VII                                 <]
Author            [> Hugo Fiennes                                <]
Date started      [> 05-March-1990                               <]
                  [>                                             <]
Module name       [> Driver calls                                <]
Current version   [> 00.04                                       <]
Version date      [> 08-January-1993                             <]
State             [> Unfinished                                  <]
                  [>                                             <]
                  [>   This source is COPYRIGHT � 1992/93 by     <]
                  [>      Hugo Fiennes of The Serial Port        <]
                  [>_____________________________________________<]
*/
// Added R2 value 0 to driver_initialise #define .. it needs defining
//  for blockdirvers noe (2004-04-21  JWB)
 
extern int  driver(int,...);
extern int  *driver_speedtable,driver_flags,driver_version,driver_noofspeeds;
extern char *driver_info,*driver_creator;
extern void driver_init(void);

extern int driver_block[1024];

extern int portnumber;

#define DRIVER_PUTBYTE        0
#define DRIVER_GETBYTE        1
#define DRIVER_PUTBLOCK       2
#define DRIVER_GETBLOCK       3
#define DRIVER_CHECKTX        4
#define DRIVER_CHECKRX        5
#define DRIVER_FLUSHTX        6
#define DRIVER_FLUSHRX        7
#define DRIVER_CONTROLLINES   8
#define DRIVER_MODEMCONTROL   9
#define DRIVER_RXERRORS      10
#define DRIVER_BREAK         11
#define DRIVER_EXAMINE       12
#define DRIVER_TXSPEED       13
#define DRIVER_RXSPEED       14
#define DRIVER_WORDFORMAT    15
#define DRIVER_FLOWCONTROL   16
#define DRIVER_INITIALISE    17
#define DRIVER_CLOSEDOWN     18
#define DRIVER_POLL          19
#define DRIVER_SELECT        20

#define driver_txspeed(tx)     driver(DRIVER_TXSPEED,portnumber,tx)
#define driver_rxspeed(rx)     driver(DRIVER_RXSPEED,portnumber,rx)
#define driver_wordformat(w)   driver(DRIVER_WORDFORMAT,portnumber,w)
#define driver_flowcontrol(f)  driver(DRIVER_FLOWCONTROL,portnumber,f)
#define driver_initialise()    (char *)(driver(DRIVER_INITIALISE,portnumber,0))
#define driver_closedown()     driver(DRIVER_CLOSEDOWN,portnumber)
#define driver_poll()          driver(DRIVER_POLL,portnumber)
#define driver_controllines(s) driver(DRIVER_CONTROLLINES,portnumber,s)
#define driver_modemcontrol(s) driver(DRIVER_MODEMCONTROL,portnumber)
#define driver_putblock(p,n)   driver(DRIVER_PUTBLOCK,portnumber,p,n)
#define driver_putbyte(c)      driver(DRIVER_PUTBYTE,portnumber,c)
#define driver_checkrx()       driver(DRIVER_CHECKRX,portnumber)
#define driver_checktx()       driver(DRIVER_CHECKTX,portnumber)
#define driver_getblock(p,n)   driver(DRIVER_GETBLOCK,portnumber,p,n)
#define driver_rxerrors()      driver(DRIVER_RXERRORS,portnumber)
