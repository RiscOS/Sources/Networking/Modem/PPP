# Copyright 1996 Acorn Computers Ltd
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#
# Makefile for PPP
#

COMPONENT   = PPP
VPATH       = pppd
OBJS        = auth blockdrive ccp chap dea_crypt dea_data fsm if_ppp ipcp lcp magic main\
              mbuf md5 minixlib options pppmodule ppp_io serial slcompress sys-riscos upap
HDRS        =
CMHGDEPENDS = if_ppp pppmodule serial main
CMHGFLAGS   = -DMemCheck_${MEMCHECK}
CFLAGS      = ${C_NOWARN_NON_ANSI_INCLUDES}
CDFLAGS     = -DDEBUG -DDEBUGLIB -DMemCheck_${MEMCHECK}
CDEFINES    = -DINET -DVJC -DDEBUGMAIN -DDEBUGCHAP -DNO_DRAND48
CINCLUDES   = -Ipppd ${TCPIPINC}
CUSTOMRES   = no
LIBS        = ${ASMUTILS} ${NET5LIBS}
SOURCES_TO_SYMLINK = $(wildcard pppd/c/*) $(wildcard pppd/h/*) $(wildcard pppd/s/*)

include CModule

# Dynamic dependencies:
